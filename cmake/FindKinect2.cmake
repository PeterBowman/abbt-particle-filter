set(Kinect2_DIR "$ENV{KINECT2_ROOT}" CACHE PATH "Path to Kinect 2.0 SDK")

if(NOT Kinect2_INCLUDE_DIR)
    find_path(Kinect2_INCLUDE_DIR NAMES Kinect.h
                                  HINTS ${Kinect2_DIR}
                                  PATH_SUFFIXES inc)
endif()

if(NOT Kinect2_LIBRARY)
    if(CMAKE_SIZEOF_VOID_P EQUAL 8)
        set(_arch x64)
    else()
        set(_arch x86)
    endif()

    find_library(Kinect2_LIBRARY NAMES Kinect20.lib
                                 HINTS ${Kinect2_DIR}
                                 PATH_SUFFIXES Lib/${_arch})
endif()

set(Kinect2_VERSION)

if(IS_DIRECTORY "${Kinect2_DIR}")
    get_filename_component(_kinect_sdk_path ${Kinect2_DIR} ABSOLUTE)
    get_filename_component(_kinect_sdk_path_parent ${Kinect2_DIR} DIRECTORY)

    string(REPLACE ${_kinect_sdk_path_parent} "" _kinect_sdk_ver ${_kinect_sdk_path})
    string(REGEX MATCH "^/?v2\\.0_([0-9]+)$" _kinect_sdk_ver_patch ${_kinect_sdk_ver})

    if(_kinect_sdk_ver_patch)
        set(Kinect2_VERSION ${CMAKE_MATCH_1})
    endif()
endif()

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(Kinect2 FOUND_VAR Kinect2_FOUND
                                          REQUIRED_VARS Kinect2_INCLUDE_DIR Kinect2_LIBRARY
                                          VERSION_VAR Kinect2_VERSION)

if(Kinect2_FOUND)
    get_filename_component(_kinect_sdk_lib_path ${Kinect2_LIBRARY} DIRECTORY)

    if(Kinect2_VERSION)
        message(STATUS "Found Kinect v2.0_${Kinect2_VERSION} in ${_kinect_sdk_lib_path}")
    else()
        message(STATUS "Found Kinect v2.0 (missing version number) in ${_kinect_sdk_lib_path}")
    endif()
endif()

set(Kinect2_INCLUDE_DIRS ${Kinect2_INCLUDE_DIR})
set(Kinect2_LIBRARIES ${Kinect2_LIBRARY})

mark_as_advanced(Kinect2_INCLUDE_DIR Kinect2_LIBRARY)
