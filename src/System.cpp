#include "System.hpp"

#include <cassert> // assert
#include <cstdlib> // std::system, std::atoi
#include <ctime> // std::clock, CLOCKS_PER_SEC

#include <ios> // std::ios::in, std::ios::out
#include <iostream>
#include <fstream> // std::ofstream
#include <string>

#include <opencv2/core.hpp> // cv::split
#include <opencv2/highgui.hpp> // cv::destroyAllWindows, cv::waitKey, cv::namedWindow, cv::resizeWindow

#include "ParticleFilter.hpp"
#include "Types.hpp"

namespace
{
    const std::string trackbarWindowName = "Parametros CIELAB";
}

const int System::EXERCISE_DURATION_SECS = 60;
const std::string System::EXPERIMENTS_FILE = "data.txt";

System::System() : started(false),
                   active(false),
                   shutdownRequested(false),
                   enableCalibration(false),
                   testDepth(false),
                   usingRightHand(true),
#ifdef ABBT_PF_EXPERIMENTAL
                   hand(false),
#endif
                   showExperiments(false),
                   enableDataDeletion(false),
                   enableBackgroundSubstraction(true),
                   debugMode(false),
                   startTime(0)
{
    while (!kinect.setup()) {}

    Cube cube = { BGR_YELLOW, HSV_YELLOW, LAB_YELLOW, "Amarillo", 0 };
    ParticleFilter f1(MAX_PARTICLES, PARTICLE_POSITION, PARTICLE_SPEED, cube, 47.5);
    filters.push_back(f1);

    cube = { BGR_RED, HSV_RED, LAB_RED, "Rojo", 0 };
    ParticleFilter f2(MAX_PARTICLES, PARTICLE_POSITION, PARTICLE_SPEED, cube, 37.5);
    filters.push_back(f2);

    cube = { BGR_BLUE, HSV_BLUE, LAB_BLUE, "Azul", 0 };
    ParticleFilter f3(MAX_PARTICLES, PARTICLE_POSITION, PARTICLE_SPEED, cube, 50.0);
    filters.push_back(f3);

    cube = { BGR_GREEN, HSV_GREEN, LAB_GREEN, "Verde", 0 };
    ParticleFilter f4(MAX_PARTICLES, PARTICLE_POSITION, PARTICLE_SPEED, cube, 20.0);
    filters.push_back(f4);

    // Lo marco como 4 porque en el proyecto no necesito más y puede dar problemas que existan más si en la Frame no se pueden marcar muchos a la vez
    assert(filters.size() == 4);

    while (!initialize()) {}

    started = true;

    if (debugMode)
    {
        createTrackbars();
    }
}

System::~System()
{
}

void System::acquireFrames()
{
    kinect.acquireColorFrame();
    kinect.acquireDepthFrame();
    kinect.detectDivider(DIVIDER_SIZE, DIVIDER_CENTER, 53.0, usingRightHand);
    kinect.prepareBoxFrame(BOX_SIZE, usingRightHand);

    if (!testDepth && enableBackgroundSubstraction)
    {
        kinect.prepareMotionFrame();
    }

#ifdef ABBT_PF_EXPERIMENTAL
    if (hand)
    {
        kinect.prepareHandColorFrame(20.0);
        kinect.prepareHandDetectionFrame();
    }
#endif

    for (int i = int(filters.size() - 1); i >= 0; i--)
    {
        filters[i].processThresholdedFrame(kinect.getFrame(Camera::FRAME_BOX));
        //filtros[i].processThresholdedFrame(kinect.getFrame(Camera::FRAME_BOX), 34.0);

        if (enableBackgroundSubstraction)
        {
            filters[i].processBackgroundSubstractionFrame(kinect.getFrame(Camera::FRAME_MOTION));
        }

        Frame handFrame;

#ifdef ABBT_PF_EXPERIMENTAL
        handFrame = kinect.getFrame(Camera::FRAME_HAND_DETECTION_AUX);
#endif

        filters[i].processMotionFrame(handFrame, enableBackgroundSubstraction);
    }
}

void System::showFrames()
{
    kinect.showFrames(debugMode);

    for (int i = 0; i < filters.size(); i++)
    {
        filters[i].showFrames(debugMode);
    }
}

void System::process()
{
    if (active)
    {
        bool hasCube = kinect.detectCube();

        for (int i = 0; i < filters.size(); i++)
        {
            if (hasCube)
            {
                filters[i].generate(kinect.getFrame(Camera::FRAME_BOX).img);
            }

            filters[i].predict(kinect.getFrame(Camera::FRAME_BOX).img);
            filters[i].estimateProbability(CUBE_SIZE);
        }

        manageFilters();

        for (int i = 0; i < filters.size(); i++)
        {
            filters[i].drawParticles(kinect.getFrame(Camera::FRAME_BOX).img);
            filters[i].drawRectangles(kinect.getFrame(Camera::FRAME_BOX).img);
            filters[i].resample();
            filters[i].drawCubeCount(cv::Point(850 + 150 * i, 350), kinect.getFrame(Camera::FRAME_COLOR).img);
        }

        kinect.drawTotalCount(BGR_WHITE);
    }
    else if (testDepth)
    {
        if (kinect.detectBox(59) && kinect.mapDepthPoint(usingRightHand))
        {
            testDepth = false;
            started = true;
        }
    }
    else
    {
        kinect.drawTotalCount(BGR_MAGENTA);

        for (int i = 0; i < filters.size(); i++)
        {
            filters[i].drawCubeCount(cv::Point(800 + 150 * i, 350), kinect.getFrame(Camera::FRAME_COLOR).img);
        }
    }
}

void System::manageFilters()
{
    for (int j = 0; j < filters[0].getGeneratedParticleSets(); j++)
    {
        int detect = 0;
        int v = 0;

        for (int i = 0; i < filters.size(); i++)
        {
            if (filters[i].isDetected(j))
            {
                detect++;
                v = i;
            }
        }

        if (detect == 1)
        {
            for (int i = 0; i < filters.size(); i++)
            {
                if (i != v)
                {
                    filters[i].markForDeletion(j);
                }
                else if (!filters[v].isMarkedAsCounted(j))
                {
                    filters[v].incrementCubeCount();

                    for (int z = 0; z < filters.size(); z++)
                    {
                        filters[z].markAsCounted(j);

                        if (filters[z].getExpiry(j) > 5)
                        {
                            filters[z].setExpiry(j, 5);
                        }
                    }
                }
            }
        }
        else if (detect == 0)
        {
            for (int i = 0; i < filters.size(); i++)
            {
                filters[i].decrementExpiry(j);

                if (filters[i].getExpiry(j) <= 0)
                {
                    filters[i].markForDeletion(j);
                }
            }
        }
    }

    for (int j = filters[0].getGeneratedParticleSets() - 1; j >= 0; j--)
    {
        int elem = 0;

        for (int i = 0; i < filters.size(); i++)
        {
            if (filters[i].isMarkedForDeletion(j))
            {
                elem++;
            }
        }

        if (elem == filters.size())
        {
            for (int i = 0; i < filters.size(); i++)
            {
                filters[i].clearAt(j);
            }
        }
    }
}

void System::promptForCalibrationStep()
{
    if (kinect.getFrame(Camera::FRAME_BOX).hasData())
    {
        std::cout << "Por favor, coloque los cubos en los lugares remarcados." << std::endl;
        std::cout << "Una vez colocados, pulse 'c' para marcar los cubos" << std::endl;

        kinect.convertBoxFrameToLab();

        for (int i = 0; i < filters.size(); i++)
        {
            int factor = 1;

#ifdef ABBT_PF_EXPERIMENTAL
            if (hand) factor = 3;
#endif

            cv::Point p;
            p.x = int(kinect.getFrame(Camera::FRAME_BOX).img.cols / 2);
            p.y = int(kinect.getFrame(Camera::FRAME_BOX).img.rows * (i + factor) / (filters.size() + factor));

            kinect.drawPoint(p, filters[i].getColor());
        }

#ifdef ABBT_PF_EXPERIMENTAL
        if (hand)
        {
            cv::Point p(int(kinect.getFrame(Camera::FRAME_BOX).img.cols / 2), int(kinect.getFrame(Camera::FRAME_BOX).img.rows / (filters.size() + 2)));
            kinect.drawPoint(p, kinect.getHandColor());
        }
#endif
    }
    else
    {
        enableCalibration = false;
    }
}

void System::calibrateCubeColors()
{
    if (kinect.getFrame(Camera::FRAME_BOX).hasData())
    {
        cv::Mat channels[3];
        cv::split(kinect.getFrame(Camera::FRAME_BOX).img, channels);

        for (int i = 0; i < filters.size(); i++)
        {
            int factor = 1;

#ifdef ABBT_PF_EXPERIMENTAL
            if (hand) factor = 3;
#endif

            cv::Point p;
            p.x = int(kinect.getFrame(Camera::FRAME_BOX).img.cols / 2);
            p.y = int(kinect.getFrame(Camera::FRAME_BOX).img.rows * (i + factor) / (filters.size() + factor));

            cv::Scalar color;

            for (int j = 0; j < 3; j++)
            {
                color[j] = channels[j].at<uchar>(p);
            }

            filters[i].setColor(color);
        }

#ifdef ABBT_PF_EXPERIMENTAL
        if (hand)
        {
            cv::Point p(int(kinect.getFrame(Camera::FRAME_BOX).img.cols / 2), int(kinect.getFrame(Camera::FRAME_BOX).img.rows / (filters.size() + 2)));
            cv::Scalar color;

            for (int j = 0; j < 3; j++)
            {
                color[j] = channels[j].at<uchar>(p);
            }

            kinect.setHandColor(color);
        }
#endif
    }
}

void System::printDepthNotice()
{
    std::cout << "Buscando la caja" << std::endl;
    std::cout << "Espere unos instantes" << std::endl;
}

void System::showInterface()
{
    std::system("cls");

    printCubeCount();

    if (debugMode)
    {
        std::cout << std::endl << "----- DEBUG -----" << std::endl << std::endl;
    }
    else
    {
        std::cout << std::endl << "-----------------" << std::endl << std::endl;
    }

    if (started)
    {
        printHelp();
    }
    else if (active)
    {
        printCountdown(EXERCISE_DURATION_SECS);
    }
    else if (enableCalibration)
    {
        promptForCalibrationStep();
    }
    else if (testDepth)
    {
        printDepthNotice();
    }
    else if (showExperiments)
    {
        printExperiments();
    }
    else if (enableDataDeletion)
    {
        promptDataDeletion();
    }
}

void System::printHelp()
{
    std::cout << "Por favor, pulse la tecla correspondiente para las siguientes opciones: " << std::endl;
    std::cout << "\tI: Iniciar ejercicio." << std::endl;
    std::cout << "\tM: Marcar cubos" << std::endl;
    std::cout << "\tP: Posicionar la caja en un lugar distinto" << std::endl;
    std::cout << "\tE: Mostrar pruebas guardadas" << std::endl;
    std::cout << "\tB: Borrar pruebas guardadas" << std::endl;
    std::cout << "\tH: Cambiar mano usada. Mano actual: " << (usingRightHand ? "derecha" : "izquierda") << std::endl;
    std::cout << "\tF: Alternar modo de sustraccion de fondo. Actual: " << (enableBackgroundSubstraction ? "habilitado" : "deshabilitado") << std::endl;
    std::cout << "\tD: Alternar modo de depuracion" << std::endl;
}

void System::printCountdown(int countdown)
{
    int seconds = countdown - (int(std::clock() - startTime) / CLOCKS_PER_SEC);

    if (seconds > 59)
    {
        std::cout << "Tiempo: 01:00";
    }
    else if (seconds > 9)
    {
        std::cout << "Tiempo: 00:" << seconds;
    }
    else if (seconds > 0)
    {
        std::cout << "Tiempo: 00:0" << seconds;
    }
    else
    {
        std::cout << "Tiempo: 00:00";
        active = false;
        started = true;
        saveExperiment();
        storeFile();
    }

    std::cout << std::endl;
}

void System::printCubeCount()
{
    std::cout << std::endl << "Cubos totales: " << kinect.getCubeCount() << std::endl << std::endl;

    for (int i = 0; i < filters.size(); i++)
    {
        std::cout << "\tCubo " << filters[i].getCubeName() << ": " << filters[i].getCubeCount() << std::endl;
    }
}

void System::createTrackbars()
{
    cv::namedWindow(trackbarWindowName, cv::WINDOW_NORMAL);
    cv::resizeWindow(trackbarWindowName, 800, 800);

    for (int i = 0; i < filters.size(); i++)
    {
        filters[i].createTrackbars(trackbarWindowName);
    }
}

void System::readKey(int key)
{
    switch (key)
    {
    case 27: // "esc" para salir
        cv::destroyAllWindows();
        shutdownRequested = true;
        break;

    case 'i': // inicializar
    case 'I':
        if (started)
        {
            while (!initialize()) {}

            startTime = std::clock();
            kinect.clearCubeCount();

            for (int i = 0; i < filters.size(); i++)
            {
                filters[i].restartCubeCount();
            }

            active = true;
            started = false;
        }
        break;

    case 's': // parar
    case 'S':
        started = true;

        if (active)
        {
            active = false;

            for (int i = 0; i < filters.size(); i++)
            {
                filters[i].clearAll();
            }
        }
        else if (enableCalibration)
        {
            enableCalibration = false;
        }
        else if (testDepth)
        {
            testDepth = false;
        }
        else if (showExperiments)
        {
            showExperiments = false;
        }
        else if (enableDataDeletion)
        {
            enableDataDeletion = false;
        }
        break;

    case 'm': // marcar el color de los cubos
    case 'M':
        if (started)
        {
            enableCalibration = true;
            started = false;
        }

        break;

    case 'c': // marcar
    case 'C':
        if (enableCalibration)
        {
            calibrateCubeColors();
            enableCalibration = false;
            started = true;
        }
        break;

    case 'h': // cambiar de mano
    case 'H':
        usingRightHand = kinect.changeHand(usingRightHand);
        break;

    case 'p': // posicionar la caja
    case 'P':
        if (started)
        {
            testDepth = true;
            started = false;
        }
        break;

    case 'e': // mostrar las pruebas
    case 'E':
        if (started)
        {
            showExperiments = true;
            started = false;
        }

    case 'b': // borrar las pruebas
    case 'B':
        if (started)
        {
            enableDataDeletion = true;
            started = false;
        }

    case 'f': // (des)habilita sustracción de fondo
    case 'F':
        enableBackgroundSubstraction = !enableBackgroundSubstraction;
        break;

    case 'd': // modo de depuración, muestra todas las imágenes
    case 'D':
        if (debugMode)
        {
            cv::destroyWindow(trackbarWindowName);
        }
        else
        {
            createTrackbars();
        }
        debugMode = !debugMode;
        break;

    default:
        break;
    }
}

bool System::initialize()
{
    if (isShutdownRequested())
    {
        return false;
    }

    kinect.acquireColorFrame();
    kinect.prepareBoxFrame(BOX_SIZE, usingRightHand);
    kinect.prepareBackgroundFrame();

    return kinect.getFrame(Camera::FRAME_BACKGROUND).hasData();

    /*kinect.acquireDepthFrame();
    kinect.mostrar_imagenes();
    return kinect.detectBox(59);*/
}

void System::saveExperiment()
{
    Experiment newExperiment;
    newExperiment.cubes.push_back(kinect.getCubeCount());
    newExperiment.names.push_back("Total");

    for (int i = 0; i < filters.size(); i++)
    {
        newExperiment.cubes.push_back(filters[i].getCubeCount());
        newExperiment.names.push_back(filters[i].getCubeName());
    }

    experiments.push_back(newExperiment);
}

void System::printExperiments()
{
    std::cout << "Pruebas realizados" << std::endl;
    std::cout << std::endl;

    for (int i = 0; i < experiments.size(); i++)
    {
        std::cout << "Prueba " << i + 1 << std::endl;
        std::cout << experiments[i].names[0] << ": " << experiments[i].cubes[0] << std::endl;

        for (int j = 1; j < experiments[i].cubes.size(); j++)
        {
            std::cout << "\tCubo " << experiments[i].names[j] << ": " << experiments[i].cubes[j] << std::endl;
        }

        std::cout << std::endl;
    }
}

void System::promptDataDeletion()
{
    std::cout << "¿Seguro que desea borrar los datos?" << std::endl;
    std::cout << "Pulse s (si) o n (no)" << std::endl;

    int c = cv::waitKey(100);

    if (c == 's' || c == 'S')
    {
        experiments.clear();
        started = true;
        enableDataDeletion = false;
    }
    else if (c == 'n' || c == 'S')
    {
        started = true;
        enableDataDeletion = false;
    }
}

void System::storeFile()
{
    std::ofstream file(EXPERIMENTS_FILE);

    for (int i = 0; i < experiments.size(); i++)
    {
        file << experiments[i].cubes.size() << std::endl;

        for (int j = 0; j < experiments[i].cubes.size(); j++)
        {
            file << experiments[i].names[j] << std::endl;
        }

        for (int j = 0; j < experiments[i].cubes.size(); j++)
        {
            file << experiments[i].cubes[j] << std::endl;
        }
    }

    file.close();
}

void System::readFromFile()
{
    std::fstream file;
    char text[40];
    file.open(EXPERIMENTS_FILE, std::ios::in);

    if (!file.is_open())
    {
        file.clear();
        file.open(EXPERIMENTS_FILE, std::ios::out); //Create file.
        file.close();
        file.open(EXPERIMENTS_FILE);
    }

    file >> text;

    while (!file.eof())
    {
        Experiment newExperiment;
        int n = std::atoi(text);
        file >> text;

        for (int i = 0; i < n; i++)
        {
            newExperiment.names.push_back(text);
            file >> text;
        }

        for (int i = 0; i < n; i++)
        {
            newExperiment.cubes.push_back(std::atoi(text));
            file >> text;
        }

        experiments.push_back(newExperiment);
    }
}
