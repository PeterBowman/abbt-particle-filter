#include <opencv2/highgui.hpp> // cv::waitKey

#include "System.hpp"

// Opciones del programa
bool compareBottom = false;
bool colorSpace[3] = { false /*RGB*/, false /*HSV*/, true /*CIELab*/ };
bool depth = true;

int main(int argc, char **argv)
{
    System sys;
    sys.readFromFile();
    //do kinect.acquireDepthFrame(); 
    //while (!kinect.detectBox(59));

    while (!sys.isShutdownRequested())
    {
        sys.acquireFrames();
        sys.process();
        sys.showInterface();
        sys.showFrames();
        sys.readKey(cv::waitKey(30));
    }

    return 0;
}
