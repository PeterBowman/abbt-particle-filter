#include "Camera.hpp"

#include <cassert> // assert
#include <cmath> // std::abs

#include <iostream>
#include <sstream> // std::ostringstream
#include <string>
#include <vector>
#include <algorithm> // std::max, std::min

#include <opencv2/core.hpp> // cv::absdiff, cv::Vec4i
#include <opencv2/imgproc.hpp> // cv::cvtColor, cv::threshold, cv::THRESH_BINARY
                               // cv::erode, cv::dilate, cv::getStructuringElement
                               // cv::MORPH_ERODE, cv::MORPH_DILATE, cv::COLOR_BGR2GRAY
                               // cv::blur, cv::Canny, cv::findContours, cv::RETR_EXTERNAL
                               // cv::CHAIN_APPROX_SIMPLE, cv::approxPolyDP, cv::arcLength
                               // cv::contourArea, cv::isContourConvex, cv::circle
                               // cv::THRESH_BINARY_INV, cv::COLOR_BGR2Lab, cv::putText
#include <opencv2/highgui.hpp> // cv::imshow, cv::destroyWindow

#include "Types.hpp"

namespace
{
    template<class Interface>
    inline void releaseHandle(Interface ** ppT)
    {
        if (*ppT)
        {
            (*ppT)->Release();
            *ppT = NULL;
        }
    }
}

const int Camera::COLOR_FRAME_WIDTH = 1920;
const int Camera::COLOR_FRAME_HEIGHT = 1080;

const int Camera::DEPTH_FRAME_WIDTH = 512;
const int Camera::DEPTH_FRAME_HEIGHT = 424;

Camera::Camera() : depthFrameReader(NULL),
                   colorFrameReader(NULL),
                   coordinateMapper(NULL),
                   depthFrameData(NULL),
                   totalCubes(0),
                   newCube(false)
{
    Frame frame;

    //0
    frame.registerWindow("Fotograma a color", true);
    frames.push_back(frame);

    //1
    frame.registerWindow("Profundidad", false);
    frames.push_back(frame);

    //2
    frame.registerWindow("Entrada de objetos", true);
    frames.push_back(frame);

    //3
    frame.registerWindow("Caja (Mano derecha)", true);
    frames.push_back(frame);

    //4
    frame.registerWindow("Fondo", false);
    frames.push_back(frame);

    //5
    frame.registerWindow("Movimiento en caja", false);
    frames.push_back(frame);

#ifdef ABBT_PF_EXPERIMENTAL
    //6 
    frame.registerWindow("Rango de mano", false);
    frames.push_back(frame);

    //7
    frame.registerWindow("Comparacion de mano", false);
    frames.push_back(frame);

    //8 
    frame.registerWindow("_Comparacion de mano", false);
    frames.push_back(frame);
#endif

    assert(frames.size() == NUM_FRAMES);

    handColor = BGR_BROWN;
    //squareMark = Point(1200, 600);
    squareMark = cv::Point(1000, 500);
}

Camera::~Camera()
{
    releaseHandle(&depthFrameReader);
    releaseHandle(&colorFrameReader);
    releaseHandle(&coordinateMapper);

    if (depthFrameData != NULL)
    {
        delete[] depthFrameData;
        depthFrameData = NULL;
    }
}

bool Camera::setup()
{
    IKinectSensor* kinect_sensor = NULL;

    //Inicializar 
    HRESULT hr = GetDefaultKinectSensor(&kinect_sensor);

    if (FAILED(hr))
    {
        return false;
    }

    hr = kinect_sensor->Open();

    if (FAILED(hr))
    {
        releaseHandle(&kinect_sensor);
        return false;
    }

    //Depth
    IDepthFrameSource* depth_source = NULL;
    hr = kinect_sensor->get_DepthFrameSource(&depth_source);

    if (FAILED(hr))
    {
        releaseHandle(&depth_source);
        releaseHandle(&kinect_sensor);
        return false;
    }

    hr = depth_source->OpenReader(&depthFrameReader);

    if (FAILED(hr))
    {
        releaseHandle(&depth_source);
        releaseHandle(&kinect_sensor);
        return false;
    }

    //Color
    IColorFrameSource* color_source = NULL;
    hr = kinect_sensor->get_ColorFrameSource(&color_source);

    if (FAILED(hr))
    {
        releaseHandle(&depth_source);
        releaseHandle(&color_source);
        releaseHandle(&kinect_sensor);
        return false;
    }

    hr = color_source->OpenReader(&colorFrameReader);

    if (FAILED(hr))
    {
        releaseHandle(&depth_source);
        releaseHandle(&color_source);
        releaseHandle(&kinect_sensor);
        return false;
    }

    hr = kinect_sensor->get_CoordinateMapper(&coordinateMapper);

    releaseHandle(&depth_source);
    releaseHandle(&color_source);
    releaseHandle(&kinect_sensor);

    return SUCCEEDED(hr);
}

void Camera::acquireColorFrame()
{
    cv::Mat color;

    //get  frame
    IColorFrame* kinect_frame = NULL;
    HRESULT hr = colorFrameReader->AcquireLatestFrame(&kinect_frame);

    if (FAILED(hr))
    {
        releaseHandle(&kinect_frame);
    }
    else
    {
        IFrameDescription* p_frame_description = NULL;
        int width = 0;
        int height = 0;
        unsigned short* p_color_buffer = NULL;

        if (SUCCEEDED(hr))
        {
            hr = kinect_frame->get_FrameDescription(&p_frame_description);
        }

        if (SUCCEEDED(hr))
        {
            p_frame_description->get_Width(&width);
            p_frame_description->get_Height(&height);
        }

        //std::cout << width << " " << height << std::endl;

        if (width == COLOR_FRAME_WIDTH && height == COLOR_FRAME_HEIGHT)
        {
            color.create(cv::Size(width, height), CV_8UC4);
            BYTE* imgDataPtr = (BYTE*)color.data;
            kinect_frame->CopyConvertedFrameDataToArray(width*height * 4, imgDataPtr, ColorImageFormat_Bgra);
        }

        releaseHandle(&kinect_frame);
        releaseHandle(&p_frame_description);
    }

    frames[FRAME_COLOR].img = color;
}

void Camera::acquireDepthFrame()
{
    cv::Mat depth_frame;

    //get depth frame
    IDepthFrame* kinect_frame = NULL;
    HRESULT hr = depthFrameReader->AcquireLatestFrame(&kinect_frame);

    if (FAILED(hr))
    {
        releaseHandle(&kinect_frame);
    }
    else
    {
        static const UINT CAPACITY = DEPTH_FRAME_WIDTH * DEPTH_FRAME_HEIGHT;

        if (depthFrameData != NULL)
        {
            delete[] depthFrameData;
            depthFrameData = NULL;
        }

        depthFrameData = new UINT16[CAPACITY];
        hr = kinect_frame->CopyFrameDataToArray(CAPACITY, depthFrameData);

        IFrameDescription* p_frame_description = NULL;
        int width = 0;
        int height = 0;

        if (SUCCEEDED(hr))
        {
            hr = kinect_frame->get_FrameDescription(&p_frame_description);
        }

        if (SUCCEEDED(hr))
        {
            p_frame_description->get_Width(&width);
            p_frame_description->get_Height(&height);
        }

        if (width == DEPTH_FRAME_WIDTH && height == DEPTH_FRAME_HEIGHT)
        {
            unsigned short depth_min_distance = 0;
            unsigned short depth_max_distance = 0;
            unsigned short* p_depth_buffer = NULL;

            //Process depth frame
            if (SUCCEEDED(hr)) hr = kinect_frame->get_DepthMinReliableDistance(&depth_min_distance);
            if (SUCCEEDED(hr)) hr = kinect_frame->get_DepthMaxReliableDistance(&depth_max_distance);

            if (SUCCEEDED(hr))
            {
                p_depth_buffer = new unsigned short[CAPACITY];
                hr = kinect_frame->CopyFrameDataToArray(CAPACITY, p_depth_buffer);

                if (SUCCEEDED(hr))
                {
                    cv::Mat depth_map(cv::Size(width, height), CV_16UC1, p_depth_buffer);
                    double scale = 255.0 / (depth_max_distance - depth_min_distance);//¿?
                    depth_map.convertTo(depth_frame, CV_8UC1, scale);//¿?
                    frames[FRAME_DEPTH].img = depth_frame;
                }
            }

            if (p_depth_buffer != NULL)
            {
                delete[] p_depth_buffer;
                p_depth_buffer = NULL;
            }
        }

        releaseHandle(&kinect_frame);
        releaseHandle(&p_frame_description);
    }
}

void Camera::prepareBackgroundFrame()
{
    if (frames[FRAME_BOX].hasData())
    {
        frames[FRAME_BOX].img.copyTo(frames[FRAME_BACKGROUND].img);
    }
}

void Camera::prepareMotionFrame()
{
    if (!frames[FRAME_BOX].hasData() || !frames[FRAME_BACKGROUND].hasData())
    {
        return;
    }

    cv::Mat grey1, grey2, temp;
    cv::cvtColor(frames[FRAME_BOX].img, grey1, cv::COLOR_BGR2GRAY);
    cv::cvtColor(frames[FRAME_BACKGROUND].img, grey2, cv::COLOR_BGR2GRAY);
    cv::absdiff(grey1, grey2, temp);
    cv::threshold(temp, temp, 20, 255, cv::THRESH_BINARY);

    cv::Mat erodeElement = cv::getStructuringElement(cv::MORPH_ERODE, cv::Size(3, 3));
    cv::erode(temp, temp, erodeElement);

    cv::Mat dilateElement = cv::getStructuringElement(cv::MORPH_DILATE, cv::Size(8, 8));
    cv::dilate(temp, frames[FRAME_MOTION].img, dilateElement);
}

bool Camera::detectBox(int thresh)
{
    if (!frames[FRAME_DEPTH].hasData())
    {
        return false;
    }

    cv::Mat temp, threshImg;
    std::vector< std::vector <cv::Point> > contours;
    std::vector<cv::Point> approx;

    frames[FRAME_DEPTH].img.copyTo(threshImg);
    cv::threshold(threshImg, threshImg, thresh, 255.0, cv::THRESH_BINARY);

    cv::blur(threshImg, temp, cv::Size(3, 3));
    cv::Canny(threshImg, temp, 80, 240, 3);

    //cv::imshow("Bordes", temp);

    // Find contours
    cv::findContours(temp, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
    int count = 0;

    for (int i = 0; i < contours.size(); i++)
    {
        // Approximate contour with accuracy proportional
        // to the contour perimeter
        cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.02, true);

        // Skip small or non-convex objects
        if (std::abs(cv::contourArea(contours[i])) < 100 || !cv::isContourConvex(approx))
        {
            continue;
        }
        else if (approx.size() == 4 && count < 1)
        {
            count++;
            cv::Point center(0, 0);

            for (int j = 0; j < approx.size(); j++)
            {
                center.x += approx[j].x;
                center.y += approx[j].y;
            }

            center.x = center.x / int(approx.size());
            center.y = center.y / int(approx.size());

            squareMark = center;
        }
        else
        {
            count++;
        }
    }

    std::cout << count << std::endl;

    if (count != 1)
    {
        return false;
    }
    else
    {
        for (int j = 0; j < approx.size(); j++)
        {
            cv::circle(threshImg, approx[j], 2, BGR_WHITE, 2);
        }

        cv::circle(threshImg, squareMark, 2, BGR_BLACK, 2);
        cv::imshow("Prueba", threshImg);
        return true;
    }
}

void Camera::prepareBoxFrame(cv::Size box, bool isRightHand)
{
    if (!frames[FRAME_COLOR].hasData())
    {
        return;
    }

    int x, y;

    if (isRightHand)
    {
        x = squareMark.x - box.width;
        y = squareMark.y - box.height / 2;
    }
    else
    {
        x = squareMark.x;
        y = squareMark.y - box.height / 2;
    }

    x = std::max(x, 0);
    x = std::min(x, frames[FRAME_COLOR].img.cols - 1 - box.width);

    y = std::max(y, 0);
    y = std::min(y, frames[FRAME_COLOR].img.rows - 1 - box.height);

    frames[FRAME_BOX].img = cropImage(frames[FRAME_COLOR].img, cv::Point(x, y), cv::Point(x + box.width, y + box.height));
}

void Camera::detectDivider(cv::Size box, cv::Point p, double sensitivity, bool isRightHand)
{
    if (!frames[FRAME_DEPTH].hasData())
    {
        return;
    }

    int x, y;

    if (isRightHand)
    {
        x = p.x - box.width + 30;
        y = p.y - box.height / 2;
    }
    else
    {
        x = p.x - 30;
        y = p.y - box.height / 2;
    }

    x = std::max(x, 0);
    x = std::min(x, frames[FRAME_DEPTH].img.cols - 1 - box.width);

    y = std::max(y, 0);
    y = std::min(y, frames[FRAME_DEPTH].img.rows - 1 - box.height);

    cv::Mat temp;
    frames[FRAME_DEPTH].img.copyTo(temp);
    temp = cropImage(temp, cv::Point(x, y), cv::Point(x + box.width, y + box.height));
    cv::threshold(temp, temp, sensitivity, 255, cv::THRESH_BINARY_INV);

    cv::Mat erodeElement = cv::getStructuringElement(cv::MORPH_ERODE, cv::Size(20, 20));
    cv::erode(temp, frames[FRAME_DIVIDER].img, erodeElement);
}

cv::Mat Camera::cropImage(cv::Mat img, cv::Point p1, cv::Point p2)
{
    cv::Mat res;
    cv::Rect zone;

    if (p1.x - 1 <= 0)
    {
        p1.x = 0;
    }
    else if (p1.x - 1 > img.cols)
    {
        p1.x = img.cols;
    }

    if (p1.y - 1 <= 0)
    {
        p1.y = 0;
    }
    else if (p1.y - 1 > img.rows)
    {
        p1.y = img.rows;
    }

    if (p2.x <= 0)
    {
        p2.x = 0;
    }
    else if (p2.x > img.cols)
    {
        p2.x = img.cols;
    }

    if (p2.y <= 0)
    {
        p2.y = 0;
    }
    else if (p2.y > img.rows)
    {
        p2.y = img.rows;
    }

    zone = cv::Rect(p1, p2);

    res = img(zone);

    //rectangle(img, zone, BGR_GREEN, 2, 0);

    return res.clone();
}

void Camera::showFrames(bool debugMode)
{
    for (int i = 0; i < frames.size(); i++)
    {
        if (!frames[i].hasData())
        {
            continue;
        }

        if (debugMode || frames[i].show)
        {
            cv::imshow(frames[i].name, frames[i].img);
        }
        else
        {
            cv::destroyWindow(frames[i].name);
        }
    }
}

bool Camera::detectCube()
{
    if (!frames[FRAME_DIVIDER].hasData())
    {
        return false;
    }

    cv::Mat temp;
    frames[FRAME_DIVIDER].img.copyTo(temp);
    //Vectores necesarios para encontrar contornos
    std::vector< std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;

    cv::findContours(temp, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE); // Devuelve los contornos externos

    if (contours.size() > 0 && !newCube)
    {
        totalCubes++;
        newCube = true;
        return true;
    }
    else if (contours.empty())
    {
        newCube = false;
        return false;
    }
    else
    {
        return false;
    }
}

void Camera::drawPoint(cv::Point p, cv::Scalar color)
{
    cv::circle(frames[FRAME_BOX].img, p, 4, color, 3);
}

void Camera::convertBoxFrameToLab()
{
    cv::cvtColor(frames[FRAME_BOX].img, frames[FRAME_BOX].img, cv::COLOR_BGR2Lab);
}

#ifdef ABBT_PF_EXPERIMENTAL
void Camera::prepareHandColorFrame(double s)
{
    if (!frames[FRAME_BOX].hasData())
    {
        return;
    }

    std::cout << std::endl;
    cv::Scalar low, up;

    for (int i = 0; i < 3; i++)
    {
        low[i] = std::max(0.0, handColor[i] - s);
        up[i] = std::min(255.0, handColor[i] + s);
    }

    cv::Mat temp;
    frames[FRAME_BOX].img.copyTo(temp);
    cv::cvtColor(frames[FRAME_BOX].img, temp, cv::COLOR_BGR2Lab);

    cv::inRange(temp, low, up, temp);

    cv::Mat erodeElement = cv::getStructuringElement(cv::MORPH_ERODE, cv::Size(8, 8));
    cv::erode(temp, temp, erodeElement);

    cv::Mat dilateElement = cv::getStructuringElement(cv::MORPH_DILATE, cv::Size(20, 20));
    cv::dilate(temp, frames[FRAME_HAND_COLOR].img, dilateElement);
}

void Camera::prepareHandDetectionFrame()
{
    if (!frames[FRAME_HAND_COLOR].hasData() || !frames[FRAME_MOTION].hasData())
    {
        return;
    }

    if (frames[FRAME_HAND_DETECTION].hasData())
    {
        frames[FRAME_HAND_DETECTION].img.copyTo(frames[FRAME_HAND_DETECTION_AUX].img);
    }
    else
    {
        frames[FRAME_HAND_DETECTION_AUX].img = 0;
    }

    //cv::imshow("0", img.img);
    cv::Mat temp;
    //cv::bitwise_not(frames[FRAME_MOTION].img, temp);
    cv::bitwise_and(frames[FRAME_HAND_COLOR].img, frames[FRAME_MOTION].img, temp);
    //cv::imshow("1", frames[FRAME_BOX].img);

    cv::Mat erodeElement = cv::getStructuringElement(cv::MORPH_ERODE, cv::Size(10, 10));
    cv::erode(temp, temp, erodeElement);
    //cv::imshow("2", frames[FRAME_BOX].img);

    cv::Mat dilateElement = cv::getStructuringElement(cv::MORPH_DILATE, cv::Size(30, 30));
    cv::dilate(temp, frames[FRAME_HAND_DETECTION].img, dilateElement);
}
#endif

bool Camera::changeHand(bool isRightHand)
{
    cv::destroyWindow(frames[FRAME_BOX].name);

    if (isRightHand)
    {
        frames[FRAME_BOX].name = " Caja (Mano izquierda)";
        return false;
    }
    else
    {
        frames[FRAME_BOX].name = " Caja (Mano derecha)";
        return true;
    }
}

void Camera::drawTotalCount(cv::Scalar color)
{
    std::ostringstream oss;
    oss << totalCubes;
    std::string text = oss.str();
    cv::putText(frames[FRAME_COLOR].img, text, cv::Point(600, 350), cv::FONT_HERSHEY_SIMPLEX, 5, color, 10);
}

bool Camera::mapDepthPoint(bool isRightHand)
{
    DepthSpacePoint depthPoint;
    depthPoint.X = float(squareMark.x);
    depthPoint.Y = float(squareMark.y);
    UINT16 value = depthFrameData[int(depthPoint.X * DEPTH_FRAME_WIDTH + depthPoint.Y)];
    ColorSpacePoint colorPoint;

    HRESULT hr = coordinateMapper->MapDepthPointToColorSpace(depthPoint, value, &colorPoint);
    //HRESULT hr = coordinateMapper->MapDepthPointsToColorSpace(1, depthPoints, depthCount, depthFrameData, colorPointCount, colorPoints);
    //HRESULT hr = coordinateMapper->MapDepthFrameToColorSpace(depthCount, depthFrameData, colorPointCount, cp);

    if (FAILED(hr))
    {
        cv::Mat temp;
        frames[FRAME_BOX].img.copyTo(temp);
        //cv::imshow("false", temp);
        return false;
    }
    else
    {
        if (isRightHand)
        {
            squareMark = cv::Point(int(colorPoint.X) + 360 / 2, int(colorPoint.Y));
        }
        else
        {
            squareMark = cv::Point(int(colorPoint.X) - 360 / 2, int(colorPoint.Y));
        }

        cv::Mat temp;
        frames[FRAME_COLOR].img.copyTo(temp);
        cv::circle(temp, squareMark, 10, BGR_BLACK, 5);
        //cv::imshow("centro", temp);
        return true;
    }
}
