#ifndef __ABBT_PF_CAMERA__
#define __ABBT_PF_CAMERA__

#include <basetsd.h> // Windows only, for UINT16
#include <vector>

#include <Kinect.h>
#include <opencv2/core.hpp>

#include "Types.hpp"

// IMAGENES A USAR

class Camera
{
public:
    enum Frames
    {
        FRAME_COLOR,              // [0]
        FRAME_DEPTH,              // [1]
        FRAME_DIVIDER,            // [2]
        FRAME_BOX,                // [3]
        FRAME_BACKGROUND,         // [4]
        FRAME_MOTION,             // [5]
#ifdef ABBT_PF_EXPERIMENTAL
        FRAME_HAND_COLOR,         // [6]
        FRAME_HAND_DETECTION,     // [7]
        FRAME_HAND_DETECTION_AUX, // [8]
#endif
        NUM_FRAMES
    };

    Camera();
    ~Camera();

    bool setup();

    void acquireColorFrame();
    void acquireDepthFrame();
    void prepareBackgroundFrame();
    void prepareMotionFrame();
    bool detectBox(int thresh);
    void prepareBoxFrame(cv::Size box, bool isRightHand);

    void detectDivider(cv::Size box, cv::Point p, double sensitivity, bool isRightHand);
    static cv::Mat cropImage(cv::Mat img, cv::Point p1, cv::Point p2);
    void showFrames(bool debugMode);

    bool detectCube();
    bool hasNewCube() { return newCube; }

    void clearCubeCount() { totalCubes = 0; }
    int getCubeCount() { return totalCubes; }
    Frame getFrame(Frames frame) { return frames[frame]; }

    void drawPoint(cv::Point p, cv::Scalar color);
    void convertBoxFrameToLab();

#ifdef ABBT_PF_EXPERIMENTAL
    void setHandColor(cv::Scalar color) { handColor = color; }
    cv::Scalar getHandColor() { return handColor; }
    void prepareHandColorFrame(double s);
    void prepareHandDetectionFrame();
#endif

    bool changeHand(bool isRightHand);

    void drawTotalCount(cv::Scalar color);

    bool mapDepthPoint(bool isRightHand);
    cv::Point getSquareMark() { return squareMark; }

private:
    std::vector<Frame> frames;

    // Para adquisición de datos
    IDepthFrameReader* depthFrameReader;
    IColorFrameReader* colorFrameReader;
    ICoordinateMapper *coordinateMapper;
    UINT16* depthFrameData;

    cv::Point squareMark; // Por realizar
    int totalCubes;
    bool newCube;

    cv::Scalar handColor;

    static const int COLOR_FRAME_WIDTH;
    static const int COLOR_FRAME_HEIGHT;
    static const int DEPTH_FRAME_WIDTH;
    static const int DEPTH_FRAME_HEIGHT;
};

#endif // __ABBT_PF_CAMERA__
