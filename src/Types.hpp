#ifndef __ABBT_PF_TYPES__
#define __ABBT_PF_TYPES__

#include <string>

#include <opencv2/core.hpp>

//Colores
const cv::Scalar BGR_RED(0, 0, 255);
const cv::Scalar BGR_BLUE(255, 0, 0);
const cv::Scalar BGR_GREEN(0, 255, 0);
const cv::Scalar BGR_CYAN(255, 255, 0);
const cv::Scalar BGR_MAGENTA(255, 0, 255);
const cv::Scalar BGR_YELLOW(0, 255, 255);
const cv::Scalar BGR_WHITE(255, 255, 255);
const cv::Scalar BGR_BLACK(0, 0, 0);
const cv::Scalar BGR_BROWN(11, 134, 184);

//Colores HSV
const cv::Scalar HSV_RED(0, 255, 255); // Comentadas las verdaderas variables HSV (0, 100, 100);
const cv::Scalar HSV_YELLOW(43, 255, 255); // (60, 100, 100);
const cv::Scalar HSV_BLUE(170, 255, 255); // (240, 100, 100);
const cv::Scalar HSV_GREEN(85, 255, 255); // (120, 100, 100);
const cv::Scalar HSV_BROWN(140, 240, 184); // (198, 94, 72);

//Colores Lab
const cv::Scalar LAB_RED(111, 177, 164);// Comentadas las verdaderas variables Lab (53.23, 80.11, 67.22);
const cv::Scalar LAB_YELLOW(246, 104, 221); // (97.14, -21.56, 94.48);
const cv::Scalar LAB_BLUE(84, 140, 82); // (32.3, 79.2, -107.86);
const cv::Scalar LAB_GREEN(82, 108, 149); // (87.74, -86.18, 83.18);
const cv::Scalar LAB_BROWN(151, 137, 190); // (59.22, 9.87, 62.73);

struct Cube
{
    cv::Scalar bgr;
    cv::Scalar hsv;
    cv::Scalar Lab;
    std::string name;
    int count;
};

struct Frame
{
    cv::Mat img;
    std::string name;
    bool show;

    bool hasData()
    {
        return img.rows != 0 && img.cols != 0;
    }

    void registerWindow(std::string name, bool show)
    {
        this->name = name;
        this->show = show;
    }
};

//bool abierto(Frame img) { if (img.img.cols != 0 && img.img.rows != 0) return true; else return false; }

//bool accesible(cv::Mat img) { if (img.cols != 0 && img.rows != 0) return true; else return false; }

#endif // __ABBT_PF_TYPES__
