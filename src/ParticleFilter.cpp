#include "ParticleFilter.hpp"

#include <cassert> // assert
#include <cstdlib> // std::rand

#include <iostream>
#include <sstream> // std::ostringstream
#include <string>
#include <vector>
#include <algorithm> // std::max, std::min

#include <opencv2/core.hpp> // cv::inRange, cv::bitwise_and, cv::bitwise_not, cv::absdiff, cv::FONT_HERSHEY_SIMPLEX
#include <opencv2/highgui.hpp> // cv::imshow, cv::destroyWindow, cv::createTrackbar
#include <opencv2/imgproc.hpp> // cv::circle, cv::rectangle, cv::cvtColor, cv::getStructuringElement
                               // cv::erode, cv::dilate, cv::MORPH_ERODE, cv::MORPH_DILATE
                               // cv::blur, cv::threshold, cv::THRESH_BINARY, cv::putText, cv::COLOR_BGR2Lab

#include "Types.hpp"

namespace
{
    inline bool withinImageBoundaries(cv::Mat img, cv::Point p)
    {
        return p.x >= 0 && p.x < img.cols && p.y >= 0 && p.y < img.rows;
    }

    inline void fitIntoRange(float &value, float minimum, float maximum)
    {
        value = std::max(value, minimum);
        value = std::min(value, maximum);
    }
}

ParticleFilter::ParticleFilter(int particles, int pos, int vel, Cube cube, double s) : cube(cube),
                                                                                       totalParticles(particles),
                                                                                       standardPosition(pos),
                                                                                       standardSpeed(vel),
                                                                                       sensitivity(s)
{
    Frame frame;

    //0
    frame.registerWindow("Rango " + cube.name, false);
    frames.push_back(frame);

    //1
    frame.registerWindow("Rango (aux) " + cube.name, false);
    frames.push_back(frame);

    //2
    frame.registerWindow("Comparacion con caja " + cube.name, false);
    frames.push_back(frame);

    //3
    frame.registerWindow("Movimiento " + cube.name, false);
    frames.push_back(frame);

    assert(frames.size() == NUM_FRAMES);
}

ParticleFilter::~ParticleFilter()
{
}

void ParticleFilter::estimateProbability(cv::Size size)
{
    for (int i = int(particleSets.size() - 1); i >= 0; i--)
    {
        if (particleSets[i].markedForDeletion)
        {
            continue;
        }

        int p_s = 0;
        int m_x = 0;
        int m_y = 0;

        for (int j = 0; j < totalParticles; j++)
        {
            cv::Point p;
            p.x = int(particleSets[i].particles[j].x);
            p.y = int(particleSets[i].particles[j].y);

            if (!withinImageBoundaries(frames[FRAME_MOTION].img, p))
            {
                continue;
            }

            bool hasOcclusion = false;

            for (int z = 0; z < particleSets.size(); z++)
            {
                if (z != i && p.inside(particleSets[i].zone))
                {
                    hasOcclusion = true;
                    break;
                }
            }

            if (hasOcclusion || frames[FRAME_MOTION].img.at<uchar>(p) != 255)
            {
                particleSets[i].particles[j].w = 0;
            }
            else
            {
                particleSets[i].particles[j].w = 1;
                p_s++;
                m_x += p.x;
                m_y += p.y;
            }
        }

        if (p_s != 0)
        {
            m_x = m_x / p_s - size.width / 2;
            m_y = m_y / p_s - size.height / 2;;
            particleSets[i].zone = cv::Rect(m_x, m_y, size.width, size.height);
        }
    }
}

void ParticleFilter::drawParticles(cv::Mat img)
{
    for (int i = 0; i < particleSets.size(); i++)
    {
        if (particleSets[i].markedForDeletion)
        {
            continue;
        }

        for (int j = 0; j < totalParticles; j++)
        {
            cv::Point p;
            p.x = int(particleSets[i].particles[j].x);
            p.y = int(particleSets[i].particles[j].y);

            if (!withinImageBoundaries(img, p))
            {
                continue;
            }

            if (particleSets[i].particles[j].w == 0)
            {
                cv::circle(img, p, 2, cube.bgr, -1, 8);
            }
            else
            {
                cv::circle(img, p, 2, BGR_BLACK, -1, 8);
            }
        }
    }
}

void ParticleFilter::drawRectangles(cv::Mat img)
{
    for (int i = 0; i < particleSets.size(); i++)
    {
        if (particleSets[i].markedForDeletion)
        {
            continue;
        }

        if (particleSets[i].zone.width != 0 && particleSets[i].zone.height != 0)
        {
            cv::rectangle(img, particleSets[i].zone, cube.bgr, 2);
        }
    }
}

void ParticleFilter::generate(cv::Mat img)
{
    ParticleSet particleSet;

    for (int j = 0; j < totalParticles; j++)
    {
        Particle particle = { float(std::rand() % img.cols),
                              float(std::rand() % img.rows),
                              0,
                              0,
                              0 };

        particleSet.particles.push_back(particle);
    }

    particleSet.detected = false;
    particleSet.markedForDeletion = false;
    particleSet.markedAsCounted = false;
    particleSet.expiry = 13;

    particleSets.push_back(particleSet);
}

void ParticleFilter::predict(cv::Mat img)
{
    for (int i = 0; i < particleSets.size(); i++)
    {
        if (particleSets[i].markedForDeletion)
        {
            continue;
        }

        std::vector<Particle> &particles = particleSets[i].particles;

        for (int j = 0; j < totalParticles; j++)
        {
            //x
            particles[j].x += standardPosition * ((float(std::rand() % 201) / 100) - 1);
            fitIntoRange(particles[j].x, 0, img.cols - 1);
            //y
            particles[j].y += standardPosition * ((float(std::rand() % 201) / 100) - 1);
            fitIntoRange(particles[j].y, 0, img.rows - 1);
            //vx
            particles[j].vx += (standardSpeed / 2) * ((float(std::rand() % 201) / 100) - 1); // Mejorar el update de las velocidades
            fitIntoRange(particles[j].vx, -3 * standardSpeed, 3 * standardSpeed);
            //vy
            particles[j].vy += (standardSpeed / 2) * ((float(std::rand() % 201) / 100) - 1);
            fitIntoRange(particles[j].vy, -3 * standardSpeed, 3 * standardSpeed);
        }
    }
}

void ParticleFilter::resample()
{
    for (int i = 0; i < particleSets.size(); i++)
    {
        if (particleSets[i].markedForDeletion)
        {
            continue;
        }

        normalizeWeights(i);

        if (!particleSets[i].detected)
        {
            continue;
        }

        for (int j = 1; j < totalParticles; j++)
        {
            particleSets[i].particles[j].w += particleSets[i].particles[j - 1].w;
        }

        std::vector<Particle> _particles = particleSets[i].particles;

        for (int j = 0; j < totalParticles; j++)
        {
            int index = findInRange(float(std::rand() % 1001) / 1000, i);
            particleSets[i].particles[j] = _particles[index];
        }
    }
}

void ParticleFilter::clearAll()
{
    particleSets.clear();
}

void ParticleFilter::clearAt(int v)
{
    particleSets.erase(particleSets.begin() + v);
}

void ParticleFilter::showFrames(bool debugMode)
{
    for (int i = 0; i < frames.size(); i++)
    {
        if (!frames[i].hasData())
        {
            continue;
        }

        if (debugMode || frames[i].show)
        {
            cv::imshow(frames[i].name, frames[i].img);
        }
        else
        {
            cv::destroyWindow(frames[i].name);
        }
    }
}

void ParticleFilter::normalizeWeights(int v)
{
    float sum = 0;

    for (int j = 0; j < totalParticles; j++)
    {
        sum += particleSets[v].particles[j].w;
    }

    if (sum != 0)
    {
        particleSets[v].detected = true;

        for (int j = 0; j < totalParticles; j++)
        {
            particleSets[v].particles[j].w /= sum;
        }
    }
    else
    {
        particleSets[v].detected = false;

        for (int j = 0; j < totalParticles; j++)
        {
            particleSets[v].particles[j].w = float(1 / totalParticles);
        }
    }
}

int ParticleFilter::findInRange(float n, int v)
{
    int ret = 0;

    for (int i = totalParticles; i > 0; i--)
    {
        if (n > particleSets[v].particles[i - 1].w)
        {
            ret = i - 1;
            break;
        }
    }

    return ret;
}

void ParticleFilter::processThresholdedFrame(Frame color, double s)
{
    if (!color.hasData())
    {
        return;
    }

    if (frames[FRAME_THRESHOLDED].hasData())
    {
        frames[FRAME_THRESHOLDED].img.copyTo(frames[FRAME_THRESHOLDED_PREV].img);
    }

    cv::Scalar low, up;

    for (int i = 0; i < 3; i++)
    {
        low[i] = std::max(0.0, cube.Lab[i] - s);
        up[i] = std::min(255.0, cube.Lab[i] + s);
    }

    cv::Mat temp;
    cv::cvtColor(color.img, temp, cv::COLOR_BGR2Lab);
    //cvtColor(img.img, temp, CV_BGR2HSV);

    cv::inRange(temp, low, up, temp);
    //cv::imshow("Prueba", temp);

    //GaussianBlur(temp, temp, Size(5, 5), 0.0);
    //cv::imshow("p" + cube.name, temp);

    cv::Mat erodeElement = cv::getStructuringElement(cv::MORPH_ERODE, cv::Size(8, 8));
    cv::erode(temp, temp, erodeElement);
    //cv::imshow("Prueba1", temp);

    cv::Mat dilateElement = getStructuringElement(cv::MORPH_DILATE, cv::Size(15, 15));
    cv::dilate(temp, frames[FRAME_THRESHOLDED].img, dilateElement);
    //cv::imshow("Prueba2", temp);
}

void ParticleFilter::processThresholdedFrame(Frame color)
{
    processThresholdedFrame(color, sensitivity);
}

void ParticleFilter::processBackgroundSubstractionFrame(Frame img)
{
    if (!frames[FRAME_THRESHOLDED].hasData() || !img.hasData())
    {
        return;
    }

    if (frames[FRAME_SUBSTRACTED].hasData())
    {
        frames[FRAME_SUBSTRACTED].img.copyTo(frames[FRAME_THRESHOLDED_PREV].img);
    }

    //cv::imshow("0", img.img);
    cv::Mat temp;
    //cv::bitwise_not(caja.img, caja.img);
    cv::bitwise_and(frames[FRAME_THRESHOLDED].img, img.img, temp);
    //cv::imshow("1", frames[FRAME_SUBSTRACTED].img);

    cv::Mat dilateElement = cv::getStructuringElement(cv::MORPH_DILATE, cv::Size(10, 10));
    cv::dilate(temp, temp, dilateElement);
    //cv::imshow("2", frames[FRAME_SUBSTRACTED].img);

    cv::Mat erodeElement = cv::getStructuringElement(cv::MORPH_ERODE, cv::Size(5, 5));
    cv::erode(temp, frames[FRAME_SUBSTRACTED].img, erodeElement);
}

void ParticleFilter::processMotionFrame(Frame hand, bool enableBackgroundSubstraction)
{
    Frame reference = enableBackgroundSubstraction ? frames[FRAME_SUBSTRACTED] : frames[FRAME_THRESHOLDED];

    if (!frames[FRAME_THRESHOLDED_PREV].hasData() || !reference.hasData())
    {
        return;
    }

    cv::Mat temp;
    cv::absdiff(frames[FRAME_THRESHOLDED_PREV].img, reference.img, temp);
    cv::bitwise_and(temp, reference.img, temp);

    if (hand.hasData())
    {
        cv::Mat tempHand;
        cv::bitwise_not(hand.img, tempHand);
        cv::bitwise_and(temp, tempHand, temp);
    }

    cv::Mat erodeElement = cv::getStructuringElement(cv::MORPH_ERODE, cv::Size(8, 8));
    cv::erode(temp, temp, erodeElement);
    /*
    Mat dilateElement = getStructuringElement(MORPH_DILATE, Size(12, 12));
    cv::erode(temp, frames[FRAME_MOTION].img, dilateElement);*/

    cv::blur(temp, temp, cv::Size(40, 40));
    cv::threshold(temp, frames[FRAME_MOTION].img, 20, 255, cv::THRESH_BINARY);
}

void ParticleFilter::drawCubeCount(cv::Point p, cv::Mat img)
{
    std::ostringstream oss;
    oss << cube.count;
    std::string text = oss.str();
    cv::putText(img, text, p, cv::FONT_HERSHEY_SIMPLEX, 3, cube.bgr, 5);
}

void ParticleFilter::createTrackbars(std::string trackbarWindowName)
{
    int L = (int)cube.Lab[0];
    int A = (int)cube.Lab[1];
    int B = (int)cube.Lab[2];

    cv::createTrackbar(cube.name + " L", trackbarWindowName, &L, 255, onTrackbarChangeL, this);
    cv::createTrackbar(cube.name + " A", trackbarWindowName, &A, 255, onTrackbarChangeA, this);
    cv::createTrackbar(cube.name + " B", trackbarWindowName, &B, 255, onTrackbarChangeB, this);
}

void ParticleFilter::onTrackbarChange(int channelId, int v, void * data)
{
    ParticleFilter * instance = static_cast<ParticleFilter *>(data);
    instance->cube.Lab[channelId] = v;
}

void ParticleFilter::onTrackbarChangeL(int v, void * data)
{
    onTrackbarChange(0, v, data);
}

void ParticleFilter::onTrackbarChangeA(int v, void * data)
{
    onTrackbarChange(1, v, data);
}

void ParticleFilter::onTrackbarChangeB(int v, void * data)
{
    onTrackbarChange(2, v, data);
}
