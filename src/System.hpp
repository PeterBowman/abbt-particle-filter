#ifndef __ABBT_PF_SYSTEM__
#define __ABBT_PF_SYSTEM__

#include <ctime>
#include <string>
#include <vector>

#include <opencv2/core.hpp>

#include "Camera.hpp"
#include "ParticleFilter.hpp"

struct Experiment
{
    std::vector<int> cubes;
    std::vector<std::string> names;
};

// Para posición de la caja
//const cv::Size BOX_SIZE(325, 325);
const cv::Size BOX_SIZE(350, 350);
const cv::Point BOX_CENTER(850, 600);
const cv::Size DIVIDER_SIZE(100, 160);
//const cv::Point DIVIDER_CENTER(240, 225);
const cv::Point DIVIDER_CENTER(240, 200);

//Para filtro de partículas
// Parámetros
const int MAX_PARTICLES = 300;
const int PARTICLE_POSITION = 30;
const int PARTICLE_SPEED = 30;
const cv::Size CUBE_SIZE(80, 80);

class System
{
public:
    System();
    ~System();

    void acquireFrames();
    void showFrames();

    void process();
    void manageFilters();

    void promptForCalibrationStep();
    void calibrateCubeColors();
    void printDepthNotice();

    void showInterface();
    void printHelp();
    void printCountdown(int countdown);
    void printCubeCount();
    void createTrackbars();

    void readKey(int key);
    bool isShutdownRequested() { return shutdownRequested; }
    bool initialize();

    void saveExperiment();
    void printExperiments();
    void promptDataDeletion();

    void storeFile();
    void readFromFile();

private:
    Camera kinect;

    bool started;
    bool active;
    bool shutdownRequested;
    bool enableCalibration;
    bool testDepth;
    bool usingRightHand;
#ifdef ABBT_PF_EXPERIMENTAL
    bool hand;
#endif
    bool showExperiments;
    bool enableDataDeletion;
    bool enableBackgroundSubstraction;
    bool debugMode;

    clock_t startTime;

    std::vector<ParticleFilter> filters;
    std::vector<Experiment> experiments;

    static const int EXERCISE_DURATION_SECS;
    static const std::string EXPERIMENTS_FILE;
};

#endif // __ABBT_PF_SYSTEM__
