#ifndef __ABBT_PF_PARTICLE_FILTER__
#define __ABBT_PF_PARTICLE_FILTER__

#include <string>
#include <vector>

#include <opencv2/core.hpp>

#include "Types.hpp"

//Estructura partícula
struct Particle
{
    float x;
    float y;
    float vx;
    float vy;
    float w;
};

class ParticleFilter
{
public:
    enum Frames
    {
        FRAME_THRESHOLDED,      // [0]
        FRAME_THRESHOLDED_PREV, // [1]
        FRAME_SUBSTRACTED,      // [2]
        FRAME_MOTION,           // [3]
        NUM_FRAMES
    };

    ParticleFilter(int particles, int pos, int vel, Cube cube, double s);
    ~ParticleFilter();

    void estimateProbability(cv::Size size);
    void drawParticles(cv::Mat img);
    void drawRectangles(cv::Mat img);
    void generate(cv::Mat img);
    void predict(cv::Mat img);
    void resample();

    void clearAll();
    void clearAt(int v);

    void showFrames(bool debugMode);

    void normalizeWeights(int v);
    int findInRange(float n, int v);

    void processThresholdedFrame(Frame color, double s);
    void processThresholdedFrame(Frame color);
    void setColor(cv::Scalar color) { cube.Lab = color; }

    cv::Scalar getColor() { return cube.Lab; }

    Frame getFrame(Frames frame) { return frames[frame]; }
    int getGeneratedParticleSets() { return int(particleSets.size()); }

    void processBackgroundSubstractionFrame(Frame img);
    void processMotionFrame(Frame hand, bool enableBackgroundSubstraction);

    bool isDetected(int v) { return particleSets[v].detected; }
    bool isMarkedForDeletion(int v) { return particleSets[v].markedForDeletion; }
    void markForDeletion(int v) { particleSets[v].markedForDeletion = true; }
    void decrementExpiry(int v) { particleSets[v].expiry--; }
    void setExpiry(int v, int n) { particleSets[v].expiry = n; }
    int getExpiry(int v) { return particleSets[v].expiry; }
    int getCount() { return cube.count; }
    bool isMarkedAsCounted(int v) { return particleSets[v].markedAsCounted; }
    void markAsCounted(int v) { particleSets[v].markedAsCounted = true; }

    std::string getCubeName() { return cube.name; }
    void incrementCubeCount() { cube.count++; }
    void restartCubeCount() { cube.count = 0; }
    int getCubeCount() { return cube.count; }

    void drawCubeCount(cv::Point p, cv::Mat img);
    void createTrackbars(std::string trackbarWindowName);

private:
    static void onTrackbarChange(int channelId, int v, void * data);
    static void onTrackbarChangeL(int v, void * data);
    static void onTrackbarChangeA(int v, void * data);
    static void onTrackbarChangeB(int v, void * data);

    struct ParticleSet
    {
        std::vector<Particle> particles;
        cv::Rect zone;
        bool detected; // object was found
        bool markedForDeletion; // delete all filters (each per color) if marked for deletion
        bool markedAsCounted;
        int expiry; // frames without variation sufficient to mark a set for deletion
    };

    std::vector<ParticleSet> particleSets;

    Cube cube;

    int totalParticles;
    int standardPosition;
    int standardSpeed;

    double sensitivity;

    std::vector<Frame> frames;
};

#endif // __ABBT_PF_PARTICLE_FILTER__
